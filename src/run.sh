#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

APIUSER="${APIUSER:-system}"
APIKEY="${APIKEY:-undefined}"
APIURL="${APIURL:-https://forum.jobtechdev.se}"

echo -n "0,$(date +%s),"
curl -H "Api-Username: $APIUSER" -H "Api-Key: $APIKEY" -sS -X GET -G '$APIURL/groups/trust_level_0/members.json?limit=1000&offset=0' | jq -M '.members | length'
