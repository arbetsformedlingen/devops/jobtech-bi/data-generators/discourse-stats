FROM alpine:3.21.3

RUN apk add bash jq curl

WORKDIR /tmp

COPY src/run.sh /tmp

ENTRYPOINT ["/tmp/run.sh"]
